# Observability

When an incident occurs human operators must use system feedback to reconstruct narratives that either explain or rule out potential causes. Logging and distributed tracing are complementary techniques for capturing system feedback. Logging's strength is recording details about individual moment-in-time events. Distributed tracing excels at correlating each high-level operation with its descendent operations, even when some operations occur on different machines, and capturing DEBUGrmation about each operation.

## Logging:

The goal of logging is to provide useful insights into the system. To that end engineers should observe the following principles:
* More significant levels of logging (ERROR, WARN, NOTICE/INFO) should be reserved for meaningful system events. Keep the signal to noise ratio high here.
* Less significant levels of logging should be used prolifically (DEBUG, TRACE) to provide any kind of information that might be useful to an engineer understanding a particular system issue

**Logging Levels**

* `FATAL`: Log for errors that force an application to shutdown.
* `ERROR`: Log for all errors in the system.
* `WARN`: Log for states that look weird in the system but that don't prevent us from operating. 
* `INFO`: Used for important system events that are not errors or warnings. The bar should be fairly high for these.
* `DEBUG`: Used for more detailed system events. Logging at this level should still be done from the perspective of "What question am I trying to answer?"
* `TRACE`: Developer-only diagnostic logs. These should be compiled out of the system at release time.

1. For incoming HTTP or gRPC requests:
    1. If the request is mutating (e.g., `PUT`, `POST`, or `DELETE`), log the URL of the request as a `INFO` event.   
        1. If the request is successful, log the details of the request (headers, request body, response body) as an `DEBUG` event.
        1. If the request fails due to a `40x` error, log the details as an `DEBUG` event.
        1. If the request fails due to a `50x` error, log the details as an `ERROR` event.
1. For outgoing HTTP or gRPC requests:
    1. If we own the software we are calling into, log the request details as an `DEBUG` event.
    1. If we don't own the software we are calling into, log the url as a `INFO` event and the details as an `DEBUG` event.
1. It would probably be helpful to have some standard logging variants for use at this level across our system.
1. It's best to implement this kind of logging as middleware.    
1. `INFO` when a transaction is rejected.
1. `INFO` when a transaction is committed. 
1. `INFO` when a making a mutating call to any external system that we don't own. For example:
    1. Mutating bank activity is performed (e.g., transfer as opposed to crawling the transaction history).
    1. Submitting transactions to Substrate
    1. Signing a transaction
    1. Anything that you would have to mock to reasonably test the system.
1. Logging mutating calls at `INFO` level does not mean that the _details_ of the call should to be logged at that level. (e.g., it's useful to log that a database or network operation occurred, but the bodies of those calls may be logged at `DEBUG` level instead.)
1. All logging at network boundaries should otherwise be at `DEBUG` level. This should include headers, the urls, and bodies of requests and responses.
1. Mutating calls into a persistence store should be logged at `DEBUG` level.
1. All other logging should be at `TRACE` level unless it is specifically designed to be a part of a dashboard (i.e., unless it answers a specific question.)

> For example, when the Member API receives a creation request in it's API, it logs the URL that was called
    as a `INFO` event and the details as an `DEBUG` event. When it calls through to the `Xand API`, since that is software we own it logs the call
    as an `DEBUG` event. When `Xand API` receives the call, it logs the url as a `INFO` event and the details as an DEBUG event. When `Xand API` makes a call to substrate--software we do not own--it logs a `INFO` event and the details as an `DEBUG` event. 

Organizing logs this way will make it possible to easily render "significant" events by filtering on `INFO` and `ERROR` events. Inter-process data can be crawled using `DEBUG` level. `TRACE` level logs can be turned on in an emergency.

## Distributed Tracing:

In the tracing community, standardization efforts are converging on [OpenTelemetry](https://opentelemetry.io/about). For those new to distributed tracing, OpenTelemetry has a helpful [overview](https://github.com/open-telemetry/opentelemetry-specification/blob/master/specification/overview.md). Standards, libraries, and services in this space are quite young compared to logging. To gracefully evolve our tracing strategy alongside community efforts we should prefer using stable, core features and concepts over those which are tentative or implementation-specific.

Baseline concepts to understand include:

- Trace
  - [W3C's definition of distributed trace](https://www.w3.org/TR/trace-context/#glossary): _A distributed trace is a set of events, triggered as a result of a single logical operation, consolidated across various components of an application. A distributed trace contains events that cross process, network and security boundaries. A distributed trace may be initiated when someone presses a button to start an action on a website - in this example, the trace will represent calls made between the downstream services that handled the chain of requests initiated by this button being pressed._
- Span
  - _A window of time during which an operation occurs along with contextual metadata._
  - A span may have zero or more children spans for sub-operations. Each child refers to its parent span by ID.
  - Spans without a parent are known as _root_ spans. Each root span and its descendant spans all belong to the same _trace_.
  - Spans belonging to the same trace or parent span may be created on different machines. Consequently, trace IDs and parent IDs must be conveyed between any components that do not share local state.

As a concrete example, every CI/CD pipeline run for [thermite](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite) produces a trace and a tree of spans for GitLab jobs and long-running commands executed within each job. Here is a visualization of temporal relationships between spans from a recent pipeline trace.

![Trace of thermite CI/CD pipeline run](/images/thermite-pipeline-trace.png)

Each span may capture metadata for a set of fields. Some fields are generally expected, including `trace.trace_id` and `trace.span_id`. Additional fields can capture more domain-specific context, much like what's done for structured logging. In the case of thermite's CI/CD traces domain-specific fields like `branch`, `name`, and `cmd` convey context which simplify analysis.

![Span data from thermite CI/CD](/images/thermite-pipeline-span-data.png)

Collecting tracing data and making it available enables us to query for patterns and outliers, create dashboards, and implement automated alerts and recovery operations. Suppose the XAND validator we maintain reports elevated latency in blocks received. We could use logs and tracing data available on that node to investigate follow-up questions like,

1. Are any operations leading up to block creation also exhibiting elevated latency?
1. Is the latency relatively consistent across all recent blocks?
1. Are blocks created by some subset of validators arriving slower than others?

Although we can speculate about ways the system will (mis)behave and potential explanations, there will be unanticipated scenarios. [Observability](https://www.honeycomb.io/blog/observability-whats-in-a-name/) is tuned for interactively discovering and understanding _unknown-unknowns_ in running systems. To enable these open-ended investigations the first step is ensuring our components are instrumented using consistent conventions.

Here is the [thermite CI/CD dashboard](https://ui.honeycomb.io/transparent-systems/board/5AJcARVag5C) (for access request an [invite](https://ui.honeycomb.io/join_team/transparent-systems) and then notify Sage).

![Thermite CI/CD dashboard](/images/thermite-pipeline-dashboard.png)

### Implementation guidelines for our products

We will need to settle on conventions for XAND and potentially other products. For now this section aims to build consensus around high-level intentions.

The XAND systems architecture documents [sample use cases](https://gitlab.com/TransparentIncDevelopment/docs/systems-architecture/-/blob/master/README.md#sample-use-cases) as [sequence diagrams](https://en.wikipedia.org/wiki/Sequence_diagram). Each high-level use case, such as creating or redeeming XAND, should produce traces and root spans. Important sub-operations, like conveying a creation request to various system components or finer grain request-response flows between any two components, should also produce spans which refer to the appropriate trace and parent span.

In general:

1. A `TraceID` and `SpanID` should appear in each request crossing a service boundary, and these essential fields should be managed by [middleware](https://en.wikipedia.org/wiki/Middleware).
   - TBD: precise conventions for each protocol and service, (e.g. particular HTTP headers)
1. A `TraceID` and `SpanID` should appear in each log.
   - TBD: trace aggregation may be implemented based on conventional logging output or by sending data to a collection agent.
1. Additional span fields should be maintained in thread-local context, so child spans can refer to the appropriate parent span, inherit the parent's fields, and potentially add more fields.
   - TBD: implementation details.

## Resources

https://www.loomsystems.com/blog/single-post/2017/01/26/9-logging-best-practices-based-on-hands-on-experience

https://www.honeycomb.io/logs/

https://en.wikipedia.org/wiki/Syslog

https://www.loggly.com/

https://brandur.org/logfmt

https://en.wikipedia.org/wiki/Tracing_(software)

https://github.com/open-telemetry/opentelemetry-specification/blob/master/specification/overview.md
