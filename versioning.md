# Versioning

This document outlines the process and conventions with which we version and publish libraries and 
components in the development process. These published artifacts end up in Artifactory, TPFS-Crates, 
and Google Container Registry (GCR), to be fed into a testing and release process.

> Note on "publish" vs "release": _publish_ is used in this writeup to specify the publishing of any 
> package, crate, or image to an internal repository like Artifactory, crates, and our `xand-dev` 
> [GCR](https://console.cloud.google.com/gcr/images/xand-dev?project=xand-dev). 
> _release_ is used to explicitly call out the publishing of a component that _would_ be consumed by 
> an external party.

We semantically version our packages, crates, and components. See [semver.org](https://semver.org/) to learn more details.

A very quick summary of our approach to semantic versioning: A version of `2.6.11-alpha.789` maps to `<MAJOR>.<MINOR>.<PATCH>-<PRERELEASE_METADATA>`. 
- `MAJOR` is incremented when introducing breaking API changes. 
- `MINOR` is incremented when adding functionality in a backwards compatible manner. 
- `PATCH` is incremented when making backwards compatible bug fixes. 
- `PRERELASE_METADATA` (optional) indicates a "pre-release" artifact, carrying with it some build metadata. In our case, the metadata is the Job ID that published the artifact.

When introducing a new feature, or a breaking API change (typically over the course of an epic or 
milestone), it is expected that a developer manually bumps the appropriate major, minor, or patch 
version. When iterating towards that milestone, feature branches will offer an optional, manual job 
to publish pre-release artifacts through the CI pipeline.

The `master` branch will consistently be publishing stable versions of a package, crate, and/or image.  

## Git Tags

We use lightweight [git tags](https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag)
to "capture a point in history that is used for a marked version". Git tags make it easy to 
view the corresponding source code for a particular version of something. 

Our tagging convention is to prepend `v` to the semantic version of the package. For example, for a `1.3.22` version
of `xand-bank-mocks`, there would be a corresponding tag of `v1.3.22`. See `xand-bank-mocks` tags 
[here](https://gitlab.com/TransparentIncDevelopment/product/testing/xand-bank-mocks/-/tags).

Repos would be aligned with one of the 2 publish processes:
- **Tag-To-Publish**
- **Publish-On-Merge-To-Master**

More mature repos, which produce core components part of a downstream testing/release process, would
follow the more manual **Tag-To-Publish** process.
 
Smaller, internal repos would follow the more automated **Publish-On-Merge** process to reduce friction
needed to publish newer versions for internal consumption.

> Top level components will follow the **Tag-To-Publish** process (`member-api`, `xand-wallet`, 
> `trustee-node`, `validator`, `xand-api`). All other components (`xand-bank-mocks`)
> and libraries will follow the more automated **Publish-On-Merge-To-Master** process.

Top-Level components can choose to ingest non-breaking changes from rapidly-iterating libraries. Or, 
more conservatively, they can pin themselves to specific versions of those dependencies.

### Tag-To-Publish

The publishing of top level components should be intentional, and managed in the context
shipping changes ready to be tested in internal environments. These top-level components' publish process
gets kicked off when a developer publishes a git `tag` on a commit on master. The CI pipeline can also 
offer a manual job to publish a git `tag` against the commit and version it is running for. This would also 
kick off the publish process.

Example:
- Latest `trustee-node` version published is `2.3.0`
- Team is working on a set of features working towards `2.4.0`
    - Along the way, pre-release versions can be manually published off of feature branches or the master branch,
    making versions like `2.4.0-alpha-<BUILD_METADATA>` available for internal consumption
- When functionality is deemed ready, Project Lead would `tag` and `push` a specific commit on master 
(manually via command line, or a manual job the CI UI)
    ```
    git checkout master && git pull
    git tag v2.4.0
    git push v2.4.0
    ```
- The pushing of the new tag would trigger a CI pipeline, publishing the stable version `2.4.0` of the component
for downstream testing and release

### Publish-On-Merge-To-Master

To encourage faster iterations across smaller, internal libraries within TPFS, we relax the requirement
to manually `git tag` to kick off the publishing of said library to Artifactory/TPFS-Crates.

Instead, a version of the library is published for every merge to master, and a git `tag` is automatically
pushed on the successful publish.

This still requires us to correctly bump the version appropriately for each MR. If the version is not bumped appropriately,
no artifact would be published off of master. 

## Practical Details

### From libraries to applications - Updating downstream consumers

Libraries do not check in their `Cargo.lock` file, whereas binaries and components _do_ check in 
their `Cargo.lock`. This FAQ answer - [**Why do binaries have Cargo.lock in version control, but not libraries**](https://doc.rust-lang.org/cargo/faq.html#why-do-binaries-have-cargolock-in-version-control-but-not-libraries) -
explains it best:

> The purpose of a Cargo.lock is to describe the state of the world at the time of a successful build. It is then used to provide deterministic builds across whatever machine is building the package by ensuring that the exact same dependencies are being compiled.
> This property is most desirable from applications and packages which are at the very end of the dependency chain (binaries). As a result, it is recommended that all binaries check in their Cargo.lock.
> 
> For libraries the situation is somewhat different. A library is not only used by the library developers, but also any downstream consumers of the library. Users dependent on the library will not inspect the library’s Cargo.lock (even if it exists).
> This is precisely because a library should not be deterministically recompiled for all users of the library.
>
> If a library ends up being used transitively by several dependencies, it’s likely that just a single copy of the library is desired (based on semver compatibility). If Cargo used all of the dependencies' Cargo.lock files, then multiple copies of the library could be used, and perhaps even a version conflict.
>  
> In other words, libraries specify semver requirements for their dependencies but cannot see the full picture. Only end products like binaries have a full picture to decide what versions of dependencies should be used.

In practice, when an internal library is updated and is still semver-compatible with the 
version specified in a downstream library, running the downstream pipeline will implicitly use the 
new version of the dependency, verifying the changes in `build` and `test`.

In this case the downstream library doesn't need to publish any new artifacts.

If the upstream version bump is not semver-compatible with the version specified downstream, and the 
downstream library must consume the new version, an MR would be required to update the dependency's 
version in `Cargo.toml`. The downstream library should also update its own version, and a new artifact 
should be published, because the tighter version constraint may affect that library's consumers.
