# Design Review

To support autonomy at all levels of our organization it is critical that we have clarity on how we make decisions and when those decisions should be considered final and ready to be acted on. Not all decisions should require a review process. However some changes need more consideration because of the impact they have on the group as a whole. The purpose of this document is to identify when a design review process is needed and how to navigate the process such that anyone on the team can initiate and drive it to completion.

## When is a design review needed?

Not every change should require a design review. Below is a list of changes where we would expect the process to be followed. Items after the + indicate that signoff from the guild representing that concern would be needed.

- Changes to an API interface, web or sdk + architecture
- Creation of a new library + security, architecture
- Creation of a new API or system component + architecture
- Changes to on-chain logic + security

## Process - How to do a design review

### Drafting Phase

The proposer(s) should:

1. Identify the problem they want to solve
1. Draft a solution to the problem by whatever means they feel most effective
1. Produce a document laying out the problem and proposed solution

### Design Proposal

Share the document in an email to `engineering@tpfs.io` with the preface "Design Review Requested" additionally drop a notification on the `#engineering` slack channel

The email should include a reasonable review period for people to submit feedback as well as where feedback is preferred. A couple days should be sufficient for small changes, for larger changes 1-2 weeks would be reasonable.

### Review Phase

During the review phase if concerns are raised or changes are proposed and after discussion a resolution satisfactory to all parties cannot be reached then the disputing parties should form a review committee. If there are no objections to the proposal at the end of the period it should be considered ratified and the change should move forward.

## Review Committee

### Committee Formation

The review committee should include the following representatives. A single person can fulfill multiple roles on the committee such as principal engineer and representative of the security guild. The total committee size should be 3-5 people. The people with the dispute are free to select the review committee within the following requirements but care should be taken not to cherry pick members that are most likely to agree with the drafters. If needed the Squad leads are available to provide guidance and recommendations.

- A principal engineer or engineering squad lead
- The team lead of the team that will be doing the work (if known)
- The chapter head or a representative for each guild that requires input
- Business stakeholder representative if the decision is relevant to network participants
- An additional engineer that is not in any of these groups (we want people at all levels to get experience with this process)

### Committee Review

The committee should meet to review the proposal as well as any requested changes or objections. The committee can iterate on the proposal as long as is necessary to reach a solution. The committee can make decisions via consensus of all members or by any mechanism acceptable to all members (i.e. We all decided to pick the color of the button by majority vote and red beat green by 3 to 1).

### Ratification Phase

At the end the committee must reach one of the following decisions.

- Ratify the proposal
- Make changes to the proposal and ratify it (for minor changes)
- Create a new proposal with a new review phase (for dramatic changes)
- Reject the proposal

If the change is ratified then it becomes part of our roadmap of planned work and future changes to it must again go through this process. Whatever the decision a follow up email should be sent to all engineering to make clear the decision that was made.

If a committee cannot reach a decision acceptable to all members then it will be escalated to the Cohort Lead. This is undesirable and should be avoided if possible.