# Version

TPFS repositories making use of Rust track stable Rust unless explicitly stated otherwise. When a new version is released on the Rust stable channel:

- The CI configuration is updated to use the new version.
- The update is announced to developers who are in charge of running `cargo update stable` on their machines.
