# Software Architecture & Testing

## Testing Strategy

Testing software is critically important to ensuring quality. Automated tests provide a lower Mean Time to Feedback (MTTF) for errors as well as enable developer's to make changes without fear of breaking things. The earlier in the SDLC that errors can be detected and corrected, the better. (See the Test Pyramid). As engineers on the platform we should practice TDD in order to generate a thorough bed of unit tests. Unit tests alone do not ensure that everything works as expected so we will need gradually more sophisticated forms of testing.

There are different approaches to testing software. This document chooses to articulate types of automated testing by the point in the SDLC at which it is executed and by what it covers. There may be different strategies for testing at each of these lifecycle points (e.g., deterministic, fuzz, property-based, load, perf, etc..)

<table>
    <thead>
        <tr>
            <th width=200px>SDLC Stage</th>
            <th>Type</th>
            <th>Target</th>
            <th width=400px>Actors</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Design / Build Time</td>
            <td>Unit</td>
            <td>Component</td>
            <td>Engineer, CI</td>
            <td>In process, no external resources. Mock at the Architectural boundaries but otherwise avoid mocks where possible.</td>
        </tr>
        <tr>
            <td>Integration</td>
            <td>Component</td>
            <td>Engineer, CI</td>
            <td>These tests will mostly target the adapters for external systems (e.g., file io, databases, 3rd party API's, 1st party API's that are not the component under test.) Integration tests are not written against real instances of external systems beyond the control of the component in question (i.e., Member API integration tests <em>will</em> run against its own database, but <em>will not</em> run against a live running instance of <code>Xand API</code></td>
        </tr>
        <tr>
            <td>Post-Deployment to Test Environment</td>
            <td>Acceptance</td>
            <td>Platform</td>
            <td>CD</td>
            <td>Largely black box, end-to-end testing. Acceptance tests <em>will</em> run against a live running instance of the entire system.</td>
        </tr>
        <tr>
            <td />
            <td>Operational Tests</td>
            <td>Platform</td>
            <td>CD</td>
            <td>
                <ul>
                    <li><b>(Performance)</b> Does the system meet its performance goals under normal circumstances?</li>
                    <li><b>(Load)</b> What does it take to topple the system?</li>
                    <li><b>(Stress)</b> How does the system recover from various kinds of failure?</li>
                    <li>other...</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Manual UX Testing</td>
            <td>Platform</td>
            <td>Engineer</td>
            <td>This testing is qualitative and pertains to the "feel" of the platform with respect to the user experience.</td>
        </tr>
        <tr>
            <td>Post-Production Release</td>
            <td>Smoke</td>
            <td>Platform</td>
            <td>Engineer</td>
            <td>A small suite of manual tests to validate production configuration.</td>
        </tr>
        <tr>
            <td></td>
            <td>Synthetic Transactcions</td>
            <td>Platform</td>
            <td>Automated</td>
            <td>Black box, end-to-end use-case testing, automated, safe for production. These tests are less about correctness and more about proving the service is running.</td>
        </tr>
        <tr>
            <td>This list is not exhaustive, but it does represent the more common cases we will encounter.</td>
        </tr>
    </tbody>
</table>

### Testing Pyramid

In general, our heaviest investment in testing should be done at the time the code is written. This means that unit tests should far outweigh other testing efforts. Why?

Unit tests are very low-cost to write and have very low Mean Time to Feedback (MTTF). This means they have the greatest ROI of any other kind of test.

This emphasis on unit testing is often represented as a [pyramid](https://automationpanda.com/2018/08/01/the-testing-pyramid/)

![Testing Pyramid](/images/testing-pyramid.png)

### TDD

[TDD](https://en.wikipedia.org/wiki/Test-driven_development) is the strongly preferred manner of writing unit tests as it ensures that all code written is necessary (required by a test) and _correct_. Engineers who are not used to writing code in a TDD style often struggle with the practice in the early stages. If this describes your experience, be satisfied with writing tests for the code you've written in the same commit.

The activity of TDD consists of three steps:

1. (RED) Write a failing unit test.
1. (GREEN) Write enough production code to make it pass.
1. (REFACTOR) Now make the code pretty.

The unit tests you write should strive to obey the [three laws of TDD](http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd):

1. Don't write any production code unless it is to make a failing unit test pass.
1. Don't write any more of a unit test than is sufficient to fail; and compilation failures are failures.
1. Don't write any more production code than is sufficient to pass the one failing unit test.

[Good unit tests](https://netobjectivesthoughts.com/good-tests-in-tdd/) have the following attributes:

1. The test must fail reliably for the reason intended.
1. The test must never fail for any other reason.
1. There must be no other test that fails for this reason.

These are ideals and practicing TDD this way is often difficult for newcomers to the practice. If this describes you then try scaling back to submitting the unit tests in the same commit as your production code. Don't forget to commit early and often!

### Further Reading

It's impossible to fully convey the scope of what you should know about test automation in this document. Below are some resources you may be interested in as you move through your career.

1. [Test Driven Development: By Example](https://www.amazon.com/Test-Driven-Development-Kent-Beck/dp/0321146530) by Kent Beck
1. [The Art of Unit Testing: 2nd Edition](https://www.amazon.com/Art-Unit-Testing-examples/dp/1617290890/ref=sr_1_1?keywords=the+art+of+unit+testing&qid=1570480565&s=books&sr=1-1) by Roy Osherove
1. [Working Effectively With Legacy Code](https://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052/ref=sr_1_1?keywords=working+effectively+with+legacy+code&qid=1570480590&s=books&sr=1-1) by Michael Feathers
1. [Refactoring: Improving the Design of Existing Code (2nd Edition)](https://www.amazon.com/Refactoring-Improving-Existing-Addison-Wesley-Signature/dp/0134757599/ref=sr_1_1?keywords=refactoring&qid=1570480620&s=books&sr=1-1) by Martin Fowler
1. [Performance vs. Load vs. Stress Testing](https://www.guru99.com/performance-vs-load-vs-stress-testing.html)

## Software Architecture

Automating tests in software has some implications for how a given piece of software is structured. Much of the friction against getting started writing test automation comes from code that is not structured for testability. As a happy coincidence, code that is highly testable also conforms to a known set of design principles called [SOLID](https://en.wikipedia.org/wiki/SOLID).

Software Architecture is the discipline related to how to structure a piece of software for all related software activites including:

1. Reading
1. Writing
1. Verification
1. Modification
1. Operation
1. Support

There are many approaches to software architecture, but they all come down to a strategy for controlling dependencies. Many impose a long list of constraints on engineers and require learning a complex taxonomy of concepts. While there may be value in many of these approaches, starting with something lightweight and simple that matches the current complexity of our needs seems like a more profitable option.

### Ports & Adapters

[Ports & Adapters](https://softwarecampament.wordpress.com/portsadapters/) is a light-weight set of architectural guidelines for managing dependencies in an application. As compared to other approaches to architecture, it imposes very little in the way of structure. It amounts to "separate the world inside my application from everything else."

In classic N-Tier architecture, a client appliction nominally communicates with an application tier--which in turn communicates with a persistence mechanism. In reality, the needs of the persistence mechanism leaked into the business layer which had implications for the client. For this reason the client had a _transitive_ dependency on the persistence tier which was hard to get rid of. This was problematic due to the [impedence mismatch](https://blog.cleancoder.com/uncle-bob/2013/10/01/Dance-You-Imps.html) between persistence needs and the needs of the domain.

![n-tier architecture](images/three-tier-software-architecture.png)

Ports & Adapters differ from this architectural model in that the business layer or `Domain` has no reference to potential clients, persistence mechanisms, or any other external component at all. In its simplest form, P&A specifies that the domain will own and define abstract `ports` which it uses to communicate to the outside world. Engineers will write `adapters` to implement the port against a particular technology or framework choice. In this way, the domain is fully insulated from the concerns of external components.

As architectural patterns go, P&A is not opinionated about much. It constrains its guidance to "push your external dependencies to the far edges of your system."

![test](images/ports-and-adapters.png)

## Guidance for Rust

### The Developer Experience

In order for tests to drive the feedback that we want, they must be ergonomic for the engineering team to use, both locally and on the build server.

### Design & Build Time

* Each component should define its own unit and integration tests.
* There should be a command executable from the repo root that will run all unit or integration tests for all components, together or separately.
* Unit and integration tests should be identifiably separate within each component–i.e., an engineer should be able to tell by the location of the test in the repo that a given test is designed for unit or integration tests.
* The same command should be executable within a component directory but run the tests scoped to that single component.
* Any setup required by unit and integration tests should be done in code as part of the test suite. Engineers should not be required to manually define environment variables, setup config files or perform any other kind of manual setup to execute these tests.
* Unit tests should be executable in parallel.
* Parallelism for Integration tests is favored but not required. This can result in false failures when doing file/io or database manipulation.

### Post-Deployment to Test Environment

* Deployment to the test environment should be automated.
* Configuration of the test environment should be automated.
* Acceptance tests should be executable against a running instance of the platform.
* Acceptance tests should be executable from the repo root.
* Any setup required by acceptance tests should be included as part of the scripts for those tests.
* Setup for acceptance tests should mirror our expectations for production. We should use the same config files in the same locations as in production. The acceptance tests should include automation for initializing the configuration on-demand. These scripts should be idempotent.
* While the normal time to run these tests is during the CI/CD process, they should be executable locally. This will be important when writing and debugging the tests.

### Post Production Release

While we do not operate the platform ourselves, it would be good of us to define a small suite of smoke tests that we can use to confirm a valid configuration of each component. We should provide these manual test scripts to our partners to help them validate their post-deployment configuration.

We should seriously consider supporting synthetic transactions on the network in order to verify platform behavior in production.

The value of synthetic transactions is that they are a kind of constant smoke test that verifies our system is functioning. A failing synthetic transaction should alert our team that attention is required immediately.

## TDD in Rust

### Engineering Values

* Code should be clean.
  * Read [Clean Code](https://smile.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882/ref=sr_1_1?keywords=clean+code&qid=1570481493&sr=8-1) for details.
* Code should be covered by automated tests.
* Tests should be relatively easy to write and understand.
  * This requires a heavy dose of _judgment_.
  * You might ask a colleague to read and make sense of your code _without explaining to them what it's for_. Take notes about what they found confusing or difficult to follow.
  * Does your code tell a story? Compare the following two implementations of the same function.

    ```rust

        // This version of the function does not tell a coherent story
        fn calculate(&self) {

            let result = 0;
            let i = 0;

            while i < 20 {
                if self.rolls[i]  == 10 {
                    result = result = self.rolls[i] + self.rolls[i+1] + self.rolls[i+2];
                    i = i + 2;
                } else if self.rolls[i] + self.rolls[i+1] == 10 {
                    result = result + self.rolls[i] + self.rolls[i+1];
                    i = i + 1;
                } else {
                    result = self.rolls[i];
                    i = i + 1;
                }
            }

            result
        }

        // this one does.
        fn score(&self) {
            let score = 0;
            let frameIndex = 0;
            let rollIndex = 0;

            while frameIndex < 10 {
                if is_strike(i) {
                    score = score + 10 + stroke_bonus(i);
                    i = i + 2;
                } else if is_spare(i) {
                    score = score + 10 + spare_bonus(i);
                    i = i + 1;
                } else {
                    score = score + self.rolls[i];
                    i = i + 1;
                }

                frameIndex = frameIndex + 1;
            }

        }

    ```

  * Does your test tell a story?

```rust
    // this test tells a story
    #[test]
    fn test_widget() {
        // Given
        let widget = Widget::new();
        let hub = MessageHub::new();
        let responder = Responder::new(&hub);

        let ticket = widget.create_ticket();

        // When
        let hub = hub.send(ticket);

        // Then
        assert_eq!(responder.tickets_processed(), 2);
    }

    // this test does NOT tell a story even though the body of
    // setup_message() and assert_tickets() may be programatically
    // identical to the first test.
    #[test]
    fn test_widget2() {
        setup_message();
        assert_tickets(2);
    }
```

* Dependencies should be configurable by the components that use them (see [Depedency Inversion Principle](https://deviq.com/dependency-inversion-principle/) and [Ports & Adapters](https://softwarecampament.wordpress.com/portsadapters/))

These are great engineering values, but how do we achieve them practically in Rust?

### Commonly Required for Unit Testing

* The component should provide a stable contract composed of traits, structs, and enums.
  * Required for Configurable Dependencies
* Structs exposed in the contract layer should be easy to construct in a test.
* All types exposed in the contract layer should `#[derive(Clone, Debug)]` so that they can be easily mocked in tests.
  * This means that types like `failure::Error` should be converted to something that is cloneable.
  * This guidance is based on limitations in current mocking frameworks available in Rust as opposed to any sort of wider design principle. If `Clone` and `Debug` are not required in order to facilitate testing, this guidance can be safely ignored.
* The contract layer should not reference any technology or framework unless it is specifically an extension for that technology or framework.

### Developer Empathy

* Every effort should be made to make the public api surface of your component as easy to use and understand as possible.
* The contract layer should minimize the use of generics.
* Obvious exceptions are `Result<T, E>` and `Option<T>`.
* Concepts like `PagedResult<T>` that are ubiquitous can also be excepted.
* Using type aliases to hide the generics does not qualify since the generic constraints still have to be understood and honored in a test.
* In general this advice amounts to "generics are nice, but harder to understand than flat types. Use with care in public facing contracts."
* If a trait exposes a `Future` as a return result, it should offer a synchronous version of the same operation. This allows the client to opt-in to futures if they need them and ignore that complexity if they don't.
  * It's true that the client can add the `.wait()` call to the end of a Future. However, an "opt-in" model is friendlier than an "opt-out" model. This attention to friendliness pays dividends as the code ages and other features are built on top of it.

### Example Hypothetical Contract Surface

```rust
#[derive(Clone, Debug)]
struct Employee {
    id: String,
    type: String,
    status: String,
    first_name: String,
    last_name: String,
    address: String,
    city: String,
    birth_date: UTC,
    // snipped for brevity
}

struct PagedResponse<T> { // exposes a generic, but the reason is warranted.
    page_number: i32,
    page_size: i32,
    items: Vec<T>
}


#[derive(Debug, Clone)]
enum MyComponentError {
    Error1(String), // If the context parameter is another struct, it must also derive Clone & Debug
    Error2(i32),
};

#[derive(Clone, Debug)]
struct EmployeesQuery {
    r#type: Option<String>,
    name: String,
    types: Vec<String>, // matches any of the specified types,
    cities: Vec<String>, // matches any of the specified cities
}

type Result<T> = std::result::Result<T, MyComponentError>; // Component level Result. Type aliasing expected here.

trait EmployeeService {
    type Employees = PagedResponse<Employee>;

    // sync version of async_get()
    fn get(id: String) -> Result<Employee>{
        async_get(id).wait();
    }

    fn get_async(id: String) -> Future<Item = Employee, Error = MyCompomentError>;

    // sync version of async_query()
    fn query(query: Option<EmployeesQuery>) -> Employees {
        async_query(id).wait()
    }

    fn query_async(query: Option<EmployeesQuery>) -> Future<Item = Transactions, Error = MyCompomentError>;

    // etc...
}
```
