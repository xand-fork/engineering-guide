# Guidelines for Installation Paths

These are the guidelines for using paths on linux based installs for our applications. It's primarily for backend services that we deploy along with CLI utilities.

These guidelines will only provide small guidance on installations for UI and desktop application installs. As for any guideline these should be the default unless there's a good reason to break out of the norm.   
These are also intended for customer based installation. Examples would be our docker images and cli programs we intend for our customer to run.

## References

These guidelines are  based on guidelines from the linux foundation. For the full specification, you can read the linux foundation [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html).  
For an easier read and an introduction to the filesystem. This article helps those from a Windows backgroud: https://www.howtogeek.com/117435/htg-explains-the-linux-directory-structure-explained/

## Installation Path

This includes any backed services that will most likely be run through a docker container.  
Default for `{binary_name}` should be the name of the app where appropriate. It can be the name of a product if it's intended to be shared by multiple applications, but this should be rare.

Without a package manager installation (usually the default for our rust installs). Executable Binaries should be placed here:  
`/usr/local/bin/{binary_name}`

When installed via a package manager like when creating a debian package `*.deb`.  
`/usr/bin/{binary_name}`  
This will happen if you create a utility application that creates a package via `cargo deb`.

## Configuration

Configuration that is intended to be use for the entire system should have its base path here and is the default for an application that should be a backend service:  
`/etc/{binary_name}`

Configuration for cli tools that are intended to be used by a particular user. Like council-voting where the JWT is issued for a particular user should be based here:  
`$HOME/.config/{binary_name}`

## Data

Anything that has persisting of data that is system wide. Should if possible be best suited at this path.  
`/var/lib/{binary_name}/data`  
There are exceptions to this rule for certain applications or extenuating circumstances to which it may not be possible to change the path.

## Logging

The actual guidelines here are to log to stdout and errors can be logged to stderr. Since our logging aggregators are intended to work from those two streams, and to avoid running out of disk that can occur from using file logging.  
If you must please accomodate for disk usage and include rules for retention, and make sure that it will be used with our log aggregators. Then the path below would be acceptabile to use:

`/var/logs/{binary_name}`

## UI Based Applications

There are some specifications around installation for UI based applications that can be adhered to though these are pretty loose since linux has a variation of display managers that have their own standards for working within their display manager.  
XDG which stands Cross-Desktop Group has some standards to help standardize what can occur between these different display managers.

### References

Here is the full specification: https://www.freedesktop.org/wiki/Specifications/


### Desktop Entry for an Application

This should live in the following folder if not installed by a package manager and should be accessible by any user:  
`/usr/local/share/applications/{binary_name}.desktop`

This should live in the following folder if installed by a package manager and should be accessible by any user:  
`/usr/share/applications/{binary_name}.desktop`

This should live in the following folder if installed for just a user (These entries will take precedence over:  
`$HOME/.local/share/applications/{binary_name}.desktop`

### UI Configuration

There are environment variables that for where configuration is intended to live in this model via `$XDG_CONFIG_HOME`, but this defaults to the same as the user configuration `$HOME/.config/` from the Configuration section of this document.  

See the Configuration section in this document for more on how configuration should work that would be the same for a desktop application.
